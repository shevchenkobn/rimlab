class GameObject {
  constructor(game) {
    if (this.constructor == GameObject) {
      throw new TypeError("Abstract class")
    }
  }
  update() { }
  draw() { }
  destroy() { }
}

class Bullet extends GameObject {
  static injects() { return [Gun, Wall] };

  static _defaults() {
    return {
      topOffset: 0,
      radius: 5,
      yStep: 20
    };
  }

  constructor(game, config) {
    super(game);
    Object.assign(this, Bullet._defaults(), config);

    this._game = game;
    this._canvas = game.$canvas;
    this._ctx = game.$ctx;

    this._isSpawned = false;
  }

  injected(injected) {
    this._gun = injected[0];
    this._wall = injected[1];
  }

  update() {
    if (!this._isSpawned) {
      return;
    }
    this.y -= this.yStep;
    if (this.y < 0 || this._wall.$destroyBrick(this)) {
      this._isSpawned = false;
    }
  }

  draw() {
    if (!this._isSpawned) {
      return;
    }
    this._ctx.beginPath();
    this._ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
    this._ctx.fillStyle = "#263238";
    this._ctx.fill();
    this._ctx.closePath();
  }

  $spawn() {
    if (this._isSpawned) {
      return;
    }
    this._isSpawned = true;
    this.x = this._gun.x + this._gun.width / 2;
    this.y = this._canvas.height - (this._gun.height + this.topOffset + this.radius);
  }
}

class Gun extends GameObject {
  static injects() { return [Bullet] };

  static _defaults() {
    return {
      leftStep: 7,
      rightStep: 7,
      height: 40,
      width: 60
    };
  }

  constructor(game, config) {
    super(game);
    Object.assign(this, Gun._defaults(), config);

    this._game = game;
    this._canvas = game.$canvas;
    this._ctx = game.$ctx;

    this.x = this._canvas.width / 2 - this.width / 2;

    this._rightPressed = false;
    this._leftPressed = false;
    this._isShooting = false;
    this._keyDownHandler = (e) => {
      switch (e.keyCode) {
        case 39:
          this._rightPressed = true;
          break;

        case 37:
          this._leftPressed = true;
          break;

        case 32:
          this._isShooting = true;
          break;
      }
    };
    this._keyUpHandler = (e) => {
      switch (e.keyCode) {
        case 39:
          this._rightPressed = false;
          break;

        case 37:
          this._leftPressed = false;
          break;

        case 32:
          this._isShooting = false;
          break;
      }
    };

    window.document.addEventListener("keydown", this._keyDownHandler, false);
    window.document.addEventListener("keyup", this._keyUpHandler, false);
  }

  injected(injected) {
    this._bullet = injected[0];
  }

  update() {
    if (this._rightPressed && this.x < this._canvas.width - this.width) {
      this.x += this.rightStep;
    }
    else if (this._leftPressed && this.x > 0) {
      this.x -= this.leftStep;
    }
    if (this._isShooting) {
      this._bullet.$spawn();
    }
  }

  draw() {
    //TODO: draw gun more detailed
    this._ctx.beginPath();
    this._ctx.rect(this.x, this._canvas.height - this.height, this.width, this.height);
    this._ctx.fillStyle = "#666";
    this._ctx.fill();
    this._ctx.closePath();
  }

  destroy() {
    window.document.removeEventListener("keydown", this._keyDownHandler);
    window.document.addEventListener("keyup", this._keyUpHandler);
  }
}

class Brick {
  constructor(wall, x, y) {
    this._wall = wall;
    this._ctx = wall.$ctx;
    this.x = x;
    this.y = y;
    this.exists = true;
  }

  draw() {
    if (!this.exists) {
      return;
    }
    this._ctx.beginPath();
    this._ctx.fillStyle = "#D07A7A";
    this._ctx.strokeStyle = "#591111";
    this._ctx.rect(this.x, this.y, this._wall._brickWidth, this._wall._brickHeight);
    this._ctx.fill();
    this._ctx.stroke();
    this._ctx.closePath();
  }
}

class Wall extends GameObject {
  static injects() { return [] };

  static _defaults() {
    return {
      offsetTop: 40,
      offsetLeft: 2,
      paddingY: 0,
      paddingX: 0,
      rows: 7,
      cols: 10
    };
  }

  constructor(game, injected, config) {
    super(game);
    Object.assign(this, Wall._defaults(), config);

    this._game = game;
    this._canvas = game.$canvas;
    this.$ctx = game.$ctx;

    this._bricks = [];
    this._brickWidth = (
      (
        this._canvas.width - this.offsetLeft * 2
        - (this.cols - 1) * this.paddingX
      )
      / this.cols
    );
    this._brickHeight = this._brickWidth / 3;
    for (let i = 0, y = this.offsetTop; i < this.rows; i++ , y += this._brickHeight + this.paddingY) {
      for (let j = 0, x = this.paddingX; j < this.cols; j++ , x += this._brickWidth + this.paddingX) {
        this._bricks.push(new Brick(this, x, y));
      }
    }

    this.fullColumn = this._brickWidth + this.paddingX;
    this.fullRow = this._brickHeight + this.paddingY;
    this.bricksYBorder = this.offsetTop + this.rows * this.fullRow - this.paddingY;
    this.bricksXBorder = this.offsetLeft + this.cols * this.fullColumn - this.paddingX;
  }

  draw() {
    for (let brick of this._bricks) {
      brick.draw();
    }
  }

  $destroyBrick(bullet) {
    const x = bullet.x;
    const yFloor = bullet.y;
    let y = bullet.y + bullet.yStep;

    while (y >= yFloor) {
      let brick = this._getExistingBrickByCoords(x, y);
      if (brick !== -1) {
        brick.exists = false;
        return true;
      } else if (brick === -2) {
        return false;
      }

      brick = this._getExistingBrickByCoords(x - bullet.radius, y);
      if (brick !== -1) {
        brick.exists = false;
        return true;
      } else if (brick === -2) {
        return false;
      }

      brick = this._getExistingBrickByCoords(x + bullet.radius, y);
      if (brick !== -1) {
        brick.exists = false;
        return true;
      } else if (brick === -2) {
        return false;
      }
      y--;
    }

    return false;
  }

  $hasCollision(x, y) {
    return typeof this._getExistingBrickByCoords(x, y) === 'object';
  }

  _getExistingBrickByCoords(x, y) {
    if (
      x < this.offsetLeft
      || x > this.bricksXBorder
    ) {
      return -2;
    }
    if (
      y < this.offsetTop
      || y > this.bricksYBorder
    ) {
      return -1;
    }

    y -= this.offsetTop;
    x -= this.offsetLeft;

    const row = Math.floor(y / this.fullRow);
    if (y - row * this.fullRow > this._brickHeight) {
      return -1;
    }
    const column = Math.floor(x / this.fullColumn);
    if (x - column * this.fullColumn > this._brickWidth) {
      return -1;
    }

    const i = row * this.cols + column;
    return this._bricks[i].exists ? this._bricks[i] : -1;
  }
}

class Minion {
  constructor(minions, x, y, vector) {
    this.minions = minions;
    this.x = x;
    this.y = y;
    this.vector = vector;

    this._colisionPoints = [];
    const angle = -this.minions.rotateDegree;
    if (angle) {
      this._colisionPoints.push(this._rotate(this.minions.width / 2, this.minions.height / 2, angle));
      this._colisionPoints.push(this._rotate(this.minions.width / 2, -this.minions.height / 2, angle));
      this._colisionPoints.push(this._rotate(-this.minions.width / 2,-this.minions.height / 2, angle));
      this._colisionPoints.push(this._rotate(-this.minions.width / 2, this.minions.height / 2, angle));
      this._actualHalfWidth = Math.max(...this._colisionPoints.map(p => p.x));
      this._actualHalfHeight = Math.max(...this._colisionPoints.map(p => p.y));
    } else {
      this._actualHalfWidth = this.minions.width / 2;
      this._actualHalfHeight = this.minions.height / 2;
    }
  }

  update() {
    this.x += this.vector.x;
    this.y += this.vector.y;

    for (let point of this._colisionPoints) {
      this._updateForCoords(this.x + point.x, this.y + point.y)
    }

    for (let point of this._colisionPoints) {
      const checkX = this.x + point.x;
      const checkY = this.y + point.y;
      if (this.minions.$wall.$hasCollision(checkX, checkY)) {
        if (checkY > this.y || checkY < this.y) {
          this.vector.y = -this.vector.y;
        }
        if (checkX > this.x || checkX < this.x) {
          this.vector.x = -this.vector.x;
        }
      }
    }
  }

  _updateForCoords(x, y) {
    if (x < 0) {
      this.x = this._actualHalfWidth;
      this.vector.x = -this.vector.x;
    }
    if (x > this.minions.$canvas.width) {
      this.x = this.minions.$canvas.width - this._actualHalfWidth;
      this.vector.x = -this.vector.x;
    }
    if (y > this.minions.$canvas.height) {
      this.y = this.minions.$canvas.height - this._actualHalfHeight;
      this.vector.y = -this.vector.y;
    }
    if (y < 0) {
      this.y = this._actualHalfHeight;
      this.vector.y = -this.vector.y;
    }
  }

  _rotate(x, y, angle) {
    return {
      x: x * Math.sin(angle) + y * Math.cos(angle),
      y: x * Math.cos(angle) + -y * Math.sin(angle)
    };
  }

  draw() {
    this.minions.$ctx.beginPath();
    this.minions.$ctx.fillStyle = "#4286f4";
    if (this.minions.rotateDegree % (2 * Math.PI)) {
      this.minions.$ctx.save();
      this.minions.$ctx.translate(this.x + this.minions.width / 2, this.y + this.minions.height / 2);
      this.minions.$ctx.rotate(this.minions.rotateDegree);
      this.minions.$ctx.fillRect(-this.minions.width / 2, -this.minions.height / 2, this.minions.width, this.minions.height);
      this.minions.$ctx.restore();
    } else {
      this.minions.$ctx.fillRect(this.x, this.y, this.minions.width, this.minions.height);
    }
    this.minions.$ctx.closePath();
  }
}

class Minions extends GameObject {
  static injects() { return [Wall, Gun] }

  static _defaults() {
    return {
      height: 10,
      width: 10,
      rotateDegree: Math.PI / 4,
      initialQuantity: 4
    };
  }

  constructor(game, config) {
    super(game);
    Object.assign(this, Minions._defaults(), config);

    this._game = game;
    this.$canvas = game.$canvas;
    this.$ctx = game.$ctx;
  }

  injected(injected) {
    this.$wall = injected[0];
    this._gun = injected[1];

    this.yLimits = [this.$wall.bricksYBorder - this.height / 2, this.$canvas.height - this._gun.height];

    this._minions = [];
    const xLimits = [0, this.$canvas.width];
    for (let i = 0; i < this.initialQuantity; i++) {
      this._minions.push(new Minion(
        this,
        this._getRandomBetween(xLimits),
        this._getRandomBetween(this.yLimits),
        this._getRandomVector()
      ));
    }
  }

  update() {
    for (let minion of this._minions) {
      minion.update();
    }
  }

  draw() {
    for (let minion of this._minions) {
      minion.draw();
    }
  }

  _getRandomBetween(limits) {
    return Math.random() * (limits[1] - limits[0]) + limits[0];
  }

  _getRandomVector(mod = 5) {
    let xFactor;
    do {
      xFactor = Math.random();
    } while (xFactor < 0.25 || xFactor > 0.75);
    xFactor = xFactor * 2 - 1;

    return {
      x: mod * xFactor,
      y: mod * (1 - xFactor)
    }
  }
}

class WallGame {
  constructor(canvasId) {
    this.$canvas = window.document.getElementById(canvasId);
    this.$ctx = this.$canvas.getContext("2d");

    this._componentTypes = [Bullet, Gun, Wall, Minions];

    this._components = [];

    this._nextFrame = window.requestAnimationFrame
      ? () => this._nextFrameId = window.requestAnimationFrame(() => this._update())
      : () => this._nextFrameId = window.setTimeout(() => this._update(), 1000 / 60);
    this._cancelNextFrame = window.cancelAnimationFrame
      ? () => window.cancelAnimationFrame(this._nextFrameId)
      : () => window.clearTimeout(this._nextFrameId)
  }

  start() {
    this.stop();

    const typeMap = new Map();
    for (let Type of this._componentTypes) {
      typeMap.set(Type, new Type(this));
    }
    this._components = [];
    for (let [Type, instance] of typeMap) {
      if (instance.injected) {
        const injections = Type.injects().map(Type => typeMap.get(Type));
        instance.injected(injections);
      }
      this._components.push(instance);
    }

    this._update();
  }

  stop() {
    this._cancelNextFrame()
    for (let component of this._components) {
      component.destroy();
    }
  }

  _update() {
    this.$ctx.clearRect(0, 0, this.$canvas.width, this.$canvas.height);
    for (let component of this._components) {
      this.$ctx.save();
      component.draw();
      this.$ctx.restore();
    }
    for (let component of this._components) {
      component.update();
    }
    this._nextFrame();
  }
}
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

// const ArcanoidStateEnum = {
//   IDLE: 0,
//   RESTART: 1,
//   PLAYING: 2,
//   FINISHED: 3,
//   LOST: 4
// };
// class WallGame {

//   constructor(canvasId) {
//     this._canvas = window.document.getElementById("myCanvas");
//     this._ctx = this._canvas.getContext("2d");

//     this._nextFrame = window.requestAnimationFrame
//       ? window.requestAnimationFrame.bind(window)
//       : (callback) => {
//         window.setTimeout(callback, 1000 / 60);
//       };

//     this._brickColors = [
//       "#aa00ff",
//       "#2962ff",
//       "#00b8d4",
//       "#00c853",
//       "#ffd600",
//       "#ff6d00",
//       "#ff5252"
//     ];
//   }

//   start() {
//     if (!this._pristine) {
//       this._resetGame();
//     }

//     window.document.addEventListener("keydown", e => this._keyDownHandler(e), false);
//     window.document.addEventListener("keyup", e => this._keyUpHandler(e), false);
//     window.document.addEventListener("mousemove", e => this._mouseMoveHandler(e), false);

//     this._gameState = ArcanoidStateEnum.IDLE;

//     this._draw();
//   }

//   _waitForStart() {
//     this._ctx.font = "60px Arial";
//     this._ctx.strokeStyle = "#f50057";
//     this._ctx.lineWidth = 4;
//     this._ctx.textAlign = 'center';
//     this._ctx.strokeText("Click to start", this._canvas.width / 2, 300);

//     const clickListener = (e) => {
//       this._canvas.removeEventListener('click', clickListener);
//       this._gameState = ArcanoidStateEnum.PLAYING;
//       this._draw();
//       this._initiateSpeedYIncrementor();
//     };
//     this._canvas.addEventListener('click', clickListener, false);
//   }

//   _keyDownHandler(e) {
//     if (this._gameState !== ArcanoidStateEnum.PLAYING) {
//       return;
//     }
//     if (e.keyCode == 39) {
//       this._rightPressed = true;
//     }
//     else if (e.keyCode == 37) {
//       this._leftPressed = true;
//     }
//   }

//   _keyUpHandler(e) {
//     if (this._gameState !== ArcanoidStateEnum.PLAYING) {
//       return;
//     }
//     if (e.keyCode == 39) {
//       this._rightPressed = false;
//     }
//     else if (e.keyCode == 37) {
//       this._leftPressed = false;
//     }
//   }

//   _mouseMoveHandler(e) {
//     if (this._gameState !== ArcanoidStateEnum.PLAYING) {
//       return;
//     }
//     let relativeX = e.clientX - this._canvas.offsetLeft;
//     if (relativeX > 0 && relativeX < this._canvas.width) {
//       this._paddleX = relativeX - this._paddleWidth / 2;
//     }
//   }

//   _resetGame() {
//     this._pristine = true;

//     this._paddleHeight = 10;
//     this._paddleWidth = 75;
//     this._ballRadius = 10;
//     this._ballX = this._canvas.width / 2;
//     this._ballY = this._canvas.height - this._paddleHeight - this._ballRadius;
//     this._setRandomSpeed();
//     this._paddleX = (this._canvas.width - this._paddleWidth) / 2;
//     this._rightPressed = false;
//     this._leftPressed = false;
//     this._brickColumnCount = 10;
//     this._brickRowCount = 7;
//     this._brickPadding = 2;
//     this._brickOffsetTop = 70;
//     this._brickOffsetLeft = 2;
//     this._brickWidth = (
//       (
//         this._canvas.width - 2
//         * this._brickOffsetLeft - (this._brickColumnCount - 1)
//         * this._brickPadding
//       )
//       / this._brickColumnCount
//     );
//     this._brickHeight = this._brickWidth / 2;
//     this._score = 0;
//     this._lives = 3;
//     this._incrementSpeedY = 0;
//     this._coordsHistory = {};

//     this._bricks = [];
//     for (let c = 0; c < this._brickRowCount; c++) {
//       this._bricks[c] = [];
//       for (let r = 0; r < this._brickColumnCount; r++) {
//         this._bricks[c][r] = { x: 0, y: 0, alive: 1 };
//       }
//     }
//   }

//   _stopSpeedIncrement() {
//     if (typeof this._speedIncrementId === 'number') {
//       window.clearInterval(this._speedIncrementId);
//       delete this._speedIncrementId;
//     }
//   }

//   _initiateSpeedYIncrementor() {
//     this._stopSpeedIncrement();
//     this._speedIncrementId = window.setInterval(
//       () => this._incrementSpeedY += 2,
//       10000
//     );
//   }

//   _setRandomSpeed(mod = 5) {
//     let xFactor;
//     do {
//       xFactor = Math.random();
//     } while (xFactor < 0.25 || xFactor > 0.75);
//     xFactor = xFactor * 2 - 1;

//     this._speedX = mod * xFactor;
//     this._speedY = -mod * (1 - (xFactor > 0 ? xFactor : -xFactor));
//   }

//   _removeBrick(brick) {
//     brick.alive = 0;
//     this._score++;
//     if (this._score == this._brickColumnCount * this._brickRowCount) {
//       this._gameState = ArcanoidStateEnum.FINISHED;
//     }
//   }

//   _drawBall() {
//     this._ctx.beginPath();
//     this._ctx.arc(this._ballX, this._ballY, this._ballRadius, 0, Math.PI * 2);
//     this._ctx.fillStyle = "#263238";
//     this._ctx.fill();
//     this._ctx.closePath();
//   }


//   _drawPaddle() {
//     this._ctx.beginPath();
//     this._ctx.rect(this._paddleX, this._canvas.height - this._paddleHeight, this._paddleWidth, this._paddleHeight);
//     this._ctx.fillStyle = "#3e2723";
//     this._ctx.fill();
//     this._ctx.closePath();
//   }


//   _drawBricks() {
//     for (let r = 0; r < this._brickRowCount; r++) {
//       for (let c = 0; c < this._brickColumnCount; c++) {
//         if (this._bricks[r][c].alive) {
//           let brickX = (c * (this._brickWidth + this._brickPadding)) + this._brickOffsetLeft;
//           let brickY = (r * (this._brickHeight + this._brickPadding)) + this._brickOffsetTop;
//           this._bricks[r][c].x = brickX;
//           this._bricks[r][c].y = brickY;
//           this._ctx.beginPath();
//           this._ctx.rect(brickX, brickY, this._brickWidth, this._brickHeight);
//           this._ctx.fillStyle = this._brickColors[r % this._brickColors.length];
//           this._ctx.fill();
//           this._ctx.closePath();
//         }
//       }
//     }
//   }

//   _drawScore() {
//     this._ctx.font = "600 16px Arial";
//     this._ctx.fillStyle = "#ff6d00";
//     this._ctx.textAlign = 'start';
//     this._ctx.textBaseline = 'top';
//     this._ctx.fillText("Score: " + this._score, 3, 3);
//     this._ctx.textBaseline = 'bottom';
//   }

//   _restartBall() {
//     this._gameState = ArcanoidStateEnum.RESTART;

//     this._ballX = this._canvas.width / 2;
//     this._ballY = this._canvas.height - this._paddleHeight - this._ballRadius;
//     this._stopSpeedIncrement();
//     this._paddleX = (this._canvas.width - this._paddleWidth) / 2;
//     this._draw();

//     this._ctx.font = "42px Arial";
//     this._ctx.strokeStyle = "#004d40";
//     this._ctx.lineWidth = 3;
//     this._ctx.textAlign = 'center';
//     this._ctx.strokeText("Click to start the ball", this._canvas.width / 2, 200);

//     const clickListener = (e) => {
//       this._gameState = ArcanoidStateEnum.PLAYING;
//       this._setRandomSpeed();
//       this._draw();
//       this._canvas.removeEventListener('click', clickListener);
//       this._initiateSpeedYIncrementor();
//     };
//     this._canvas.addEventListener('click', clickListener, false);
//   }

//   _drawLives() {
//     this._ctx.font = "600 16px Arial";
//     this._ctx.fillStyle = "#ff6d00";
//     this._ctx.textAlign = 'end';
//     this._ctx.textBaseline = 'top';
//     this._ctx.fillText("Lives: " + this._lives, this._canvas.width - 3, 3);
//     this._ctx.textBaseline = 'bottom';
//   }

//   _bounceBall() {
//     if (this._ballX + this._speedX > this._canvas.width - this._ballRadius || this._ballX + this._speedX < this._ballRadius) { // walls bounce
//       this._speedX = -this._speedX;
//     }
//     if (this._ballY + this._speedY < this._ballRadius) {
//       this._speedY = -this._speedY;
//     } else if ( // paddle bounce
//       this._ballX >= this._paddleX
//       && this._ballX <= this._paddleX + this._paddleWidth
//       && this._ballY + this._ballRadius > this._canvas.height - this._paddleHeight
//     ) {
//       this._speedY = -(this._speedY + this._incrementSpeedY);
//       this._incrementSpeedY = 0;
//       const factor = Math.abs((this._ballY + this._ballRadius - (this._canvas.height - this._paddleHeight)) / this._speedY);
//       this._ballX += factor * this._speedX
//       if (this._ballX - this._ballRadius <= 0) {
//         this._ballX = this._ballRadius + 1;
//       } else if (this._ballX + this._ballRadius >= this._canvas.width) {
//         this._ballX = this._canvas.width - this._ballRadius - 1;
//       }
//       this._ballY = this._canvas.height - this._paddleHeight - this._ballRadius;
//       // change acceleration
//       const paddleXDelta = this._coordsHistory.paddleX - this._paddleX;
//       this._speedX += -Math.sign(paddleXDelta) * Math.sqrt(paddleXDelta > 0 ? paddleXDelta : -paddleXDelta);
//     } else if (this._ballY > this._canvas.height - this._ballRadius) { // floor hit
//       this._lives--;
//       if (!this._lives) {
//         this._gameState = ArcanoidStateEnum.LOST;
//       }
//       else {
//         this._restartBall();
//       }
//     }
//   }

//   _movePaddleAndBall() {
//     this._coordsHistory.paddleX = this._paddleX
//     if (this._rightPressed && this._paddleX < this._canvas.width - this._paddleWidth) {
//       this._paddleX += 7;
//     }
//     else if (this._leftPressed && this._paddleX > 0) {
//       this._paddleX -= 7;
//     }

//     this._coordsHistory.ballX = this._ballX;

//     if (!this._detectBrickCollision()) {
//       this._ballX += this._speedX;
//       this._ballY += this._speedY;
//     }

//     // this._paddleX = this._ballX - this._paddleWidth / 2; // FIXME: for demo
//   }

//   _getBrickByCoords(x, y) {
//     const fullRow = this._brickHeight + this._brickPadding;
//     const fullColumn = this._brickWidth + this._brickPadding;
//     if (
//       y < this._brickOffsetTop
//       || y > this._brickOffsetTop + this._brickRowCount * fullRow - this._brickPadding
//       || x < this._brickOffsetLeft
//       || x > this._brickOffsetLeft + this._brickColumnCount * fullColumn - this._brickPadding
//     ) {
//       return null;
//     }

//     y -= this._brickOffsetTop;
//     x -= this._brickOffsetLeft;

//     const row = Math.floor(y / fullRow);
//     if (y - row * fullRow > this._brickHeight) {
//       return null;
//     }
//     const column = Math.floor(x / fullColumn);
//     if (x - column * fullColumn > this._brickWidth) {
//       return null;
//     }

//     return this._bricks[row][column];
//   }

//   _draw() {
//     switch (this._gameState) {
//       case ArcanoidStateEnum.LOST:
//         this._loseGame();
//         return;
//     }

//     this._ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
//     this._drawBricks();
//     this._drawBall();
//     this._drawPaddle();
//     this._drawScore();
//     this._drawLives();
//     // this._detectBrickCollision();

//     this._bounceBall();

//     switch (this._gameState) {
//       case ArcanoidStateEnum.PLAYING:
//         this._movePaddleAndBall();
//         this._nextFrame(() => this._draw());
//         break;

//       case ArcanoidStateEnum.LOST:
//         this._nextFrame(() => this._draw());
//         break;

//       case ArcanoidStateEnum.IDLE:
//         this._waitForStart();
//         break;

//       case ArcanoidStateEnum.FINISHED:
//         this._winGame();
//         break
//     }
//   }

//   _winGame() {
//     this._stopSpeedIncrement();

//     this._ctx.font = "600 80px Arial";
//     this._ctx.fillStyle = "#e53935";
//     this._ctx.lineWidth = 4;
//     this._ctx.textAlign = 'center';
//     this._ctx.fillText("YOU WON", this._canvas.width / 2, 300);

//     this._ctx.font = "42px Arial";
//     this._ctx.strokeStyle = "#039be5";
//     this._ctx.lineWidth = 2;
//     this._ctx.textAlign = 'center';
//     this._ctx.strokeText("Click to restart", this._canvas.width / 2, 500);

//     this._resetGame();
//     const clickListener = (e) => {
//       this._canvas.removeEventListener('click', clickListener);
//       this._gameState = ArcanoidStateEnum.PLAYING;
//       this._draw();
//       this._initiateSpeedYIncrementor();
//     };
//     this._canvas.addEventListener('click', clickListener, false);
//   }

//   _loseGame() {
//     this._stopSpeedIncrement();

//     this._ctx.font = "600 80px Arial";
//     this._ctx.fillStyle = "#111";
//     this._ctx.lineWidth = 4;
//     this._ctx.textAlign = 'center';
//     this._ctx.fillText("YOU LOST", this._canvas.width / 2, 300);

//     this._ctx.font = "42px Arial";
//     this._ctx.strokeStyle = "#039be5";
//     this._ctx.lineWidth = 2;
//     this._ctx.textAlign = 'center';
//     this._ctx.strokeText("Click to try again", this._canvas.width / 2, 500);

//     this._resetGame();
//     const clickListener = (e) => {
//       this._canvas.removeEventListener('click', clickListener);
//       this._gameState = ArcanoidStateEnum.PLAYING;
//       this._draw();
//       this._initiateSpeedYIncrementor();
//     };
//     this._canvas.addEventListener('click', clickListener, false);
//   }
// }

const game = new WallGame("myCanvas");
game.start();